<?php
/**
 * Created by IntelliJ IDEA.
 * User: Luis
 * Date: 09/10/2018
 * Time: 13:14
 */
?>
 <fieldset>
  <div class="panel-localidad">
    <h2 style="font-size: 20px;text-align: center" class="vc_custom_heading">¿Qué te interesa?</h2>
    <form action="javascript:0">
    <div class="wpb_wrapper">
    <!-- --------------------------- Internet ------------------------------ -->
    <div class="accordion-internet" id="accordion">
      <div class="panel-accordion">
        <!-- --------------------------- Internet ------------------------------ -->
        <a data-toggle="collapse" data-parent="#accordion" data-target="#collapse1"
           aria-expanded="true" id="expanded-internet">
          <div class="panel-heading card-header">
            <h4 class="panel-title"><label class="tn-headline">
                <div id="caja"><img
                    src="<?php echo plugins_url().'/wp-configurador/ico/Line-17.png'?>" alt="Sikarra internet">
                  <p class="card-title-catg">INTERNET</p></div>
              </label></h4>
            <span style="float:right; top:0rem; margin-top:-47px;"><input class="input-acordeon" name="conf[]" type="checkbox" id="switch1" required><label class="label-acordeon" for="switch1">Toggle</label></span>
          </div>
        </a>
        <div class="collapse" id="collapse1">
          <div class="card card-body">
            <div id="grupofibra1" class="grupofibra">
              <div class="col selectwithhtml">
                <label>
                  <input type="radio" name="fibra" value="cien" class="hideradio">
                  <div
                    class="vc_col-sm-4 wpb_column column_container vc_column_container col boxed centered-text has-animation padding-4-percent animated-in instance-6"
                    data-t-w-inherits="default" data-shadow="none" data-border-radius="none" data-border-animation=""
                    data-border-animation-delay="" data-border-width="none" data-border-style="solid" data-border-color=""
                    data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1"
                    data-hover-bg="" data-hover-bg-opacity="1" data-animation="grow-in" data-delay="0"
                    style="transform: scale(1, 1); opacity: 1;">
                    <div class="column-bg-overlay"></div>
                    <div class="vc_column-inner">
                      <div class="wpb_wrapper">

                        <div class="wpb_text_column wpb_content_element">
                          <div class="wpb_wrapper">
                            <h5>Internet Fibra Òptica 100</h5>
                          </div>
                        </div>


                        <div class="divider-wrap" data-alignment="default">
                          <div style="height: 25px;" class="divider"></div>
                        </div>
                        <div class="wpb_text_column wpb_content_element">
                          <div class="wpb_wrapper">
                            <p>Des de</p>
                          </div>
                        </div>


                        <div class="nectar-gradient-text precio" data-direction="horizontal" data-color="extra-color-gradient-1" style="">
                          <h2>30,00 €</h2></div>
                        <div class="nectar-gradient-text" data-direction="horizontal" data-color="extra-color-gradient-1"
                             style="margin-top: -40px; "><h6>/mes</h6></div>
                        <div class="wpb_text_column wpb_content_element">
                          <div class="wpb_wrapper">
                            <p>Velocitats fins a 100 Mbps</p>
                          </div>
                        </div>

                        <div class="divider-wrap" data-alignment="default">
                          <div style="height: 25px;" class="divider"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </label><label><input type="radio" name="fibra" value="tres" class="hideradio">
                  <div
                    class="vc_col-sm-4 wpb_column column_container vc_column_container col boxed centered-text has-animation padding-4-percent animated-in instance-7"
                    data-t-w-inherits="default" data-shadow="none" data-border-radius="none" data-border-animation=""
                    data-border-animation-delay="" data-border-width="none" data-border-style="solid" data-border-color=""
                    data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1"
                    data-hover-bg="" data-hover-bg-opacity="1" data-animation="grow-in" data-delay="0"
                    style="transform: scale(1, 1); opacity: 1;">
                    <div class="column-bg-overlay"></div>
                    <div class="vc_column-inner">
                      <div class="wpb_wrapper">

                        <div class="wpb_text_column wpb_content_element ">
                          <div class="wpb_wrapper">
                            <h5>Internet Fibra Òptica 300</h5>
                          </div>
                        </div>


                        <div class="divider-wrap" data-alignment="default">
                          <div style="height: 25px;" class="divider"></div>
                        </div>
                        <div class="wpb_text_column wpb_content_element  vc_custom_1587921730419">
                          <div class="wpb_wrapper">
                            <p>Des de</p>
                          </div>
                        </div>


                        <div class="nectar-gradient-text precio" data-direction="horizontal" data-color="extra-color-gradient-1" style="">
                          <h2>35,00 €</h2></div>
                        <div class="nectar-gradient-text" data-direction="horizontal" data-color="extra-color-gradient-1"
                             style="margin-top: -40px; "><h6>/mes</h6></div>
                        <div class="wpb_text_column wpb_content_element  vc_custom_1612463045732">
                          <div class="wpb_wrapper">
                            <p>Velocitats fins a 300 Mbps</p>
                          </div>
                        </div>

                        <div class="divider-wrap" data-alignment="default">
                          <div style="height: 25px;" class="divider"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </label><label><input type="radio" name="fibra" value="rdoce" class="hideradio">
                  <div
                    class="vc_col-sm-4 wpb_column column_container vc_column_container col boxed centered-text has-animation padding-4-percent animated-in instance-8"
                    data-t-w-inherits="default" data-shadow="none" data-border-radius="none" data-border-animation=""
                    data-border-animation-delay="" data-border-width="none" data-border-style="solid" data-border-color=""
                    data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1"
                    data-hover-bg="" data-hover-bg-opacity="1" data-animation="grow-in" data-delay="0"
                    style="transform: scale(1, 1); opacity: 1;">
                    <div class="column-bg-overlay"></div>
                    <div class="vc_column-inner">
                      <div class="wpb_wrapper">

                        <div class="wpb_text_column wpb_content_element ">
                          <div class="wpb_wrapper">
                            <h5>Internet Rural</h5>
                          </div>
                        </div>


                        <div class="divider-wrap" data-alignment="default">
                          <div style="height: 25px;" class="divider"></div>
                        </div>
                        <div class="wpb_text_column wpb_content_element  vc_custom_1587921730419">
                          <div class="wpb_wrapper">
                            <p>Des de</p>
                          </div>
                        </div>


                        <div class="nectar-gradient-text precio" data-direction="horizontal" data-color="extra-color-gradient-1" style="">
                          <h2>24,95 €</h2></div>
                        <div class="nectar-gradient-text" data-direction="horizontal" data-color="extra-color-gradient-1"
                             style="margin-top: -40px; "><h6>/mes</h6></div>
                        <div class="wpb_text_column wpb_content_element  vc_custom_1612463045732">
                          <div class="wpb_wrapper">
                            <p>Velocitats fins a x Mbps</p>
                          </div>
                        </div>

                        <div class="divider-wrap" data-alignment="default">
                          <div style="height: 25px;" class="divider"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </label><label><input type="radio" name="fibra" value="rtreintaydos" class="hideradio">
                  <div
                    class="vc_col-sm-4 wpb_column column_container vc_column_container col boxed centered-text has-animation padding-4-percent animated-in instance-9"
                    data-t-w-inherits="default" data-shadow="none" data-border-radius="none" data-border-animation=""
                    data-border-animation-delay="" data-border-width="none" data-border-style="solid" data-border-color=""
                    data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1"
                    data-hover-bg="" data-hover-bg-opacity="1" data-animation="grow-in" data-delay="0"
                    style="transform: scale(1, 1); opacity: 1;">
                    <div class="column-bg-overlay"></div>
                    <div class="vc_column-inner">
                      <div class="wpb_wrapper">

                        <div class="wpb_text_column wpb_content_element ">
                          <div class="wpb_wrapper">
                            <h5>Internet Rural</h5>
                          </div>
                        </div>


                        <div class="divider-wrap" data-alignment="default">
                          <div style="height: 25px;" class="divider"></div>
                        </div>
                        <div class="wpb_text_column wpb_content_element  vc_custom_1587921730419">
                          <div class="wpb_wrapper">
                            <p>Des de</p>
                          </div>
                        </div>


                        <div class="nectar-gradient-text precio" data-direction="horizontal" data-color="extra-color-gradient-1" style="">
                          <h2>35,95 €</h2></div>
                        <div class="nectar-gradient-text" data-direction="horizontal" data-color="extra-color-gradient-1"
                             style="margin-top: -40px; "><h6>/mes</h6></div>
                        <div class="wpb_text_column wpb_content_element  vc_custom_1612463045732">
                          <div class="wpb_wrapper">
                            <p>Velocitats fins a x Mbps</p>
                          </div>
                        </div>

                        <div class="divider-wrap" data-alignment="default">
                          <div style="height: 25px;" class="divider"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </label><label><input type="radio" name="fibra" value="rcincuenta" class="hideradio">
                  <div
                    class="vc_col-sm-4 wpb_column column_container vc_column_container col boxed centered-text has-animation padding-4-percent animated-in instance-10"
                    data-t-w-inherits="default" data-shadow="none" data-border-radius="none" data-border-animation=""
                    data-border-animation-delay="" data-border-width="none" data-border-style="solid" data-border-color=""
                    data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1"
                    data-hover-bg="" data-hover-bg-opacity="1" data-animation="grow-in" data-delay="0"
                    style="transform: scale(1, 1); opacity: 1;">
                    <div class="column-bg-overlay"></div>
                    <div class="vc_column-inner">
                      <div class="wpb_wrapper">

                        <div class="wpb_text_column wpb_content_element ">
                          <div class="wpb_wrapper">
                            <h5>Internet Rural</h5>
                          </div>
                        </div>


                        <div class="divider-wrap" data-alignment="default">
                          <div style="height: 25px;" class="divider"></div>
                        </div>
                        <div class="wpb_text_column wpb_content_element  vc_custom_1587921730419">
                          <div class="wpb_wrapper">
                            <p>Des de</p>
                          </div>
                        </div>


                        <div class="nectar-gradient-text precio" data-direction="horizontal" data-color="extra-color-gradient-1" style="">
                          <h2>54,95 €</h2></div>
                        <div class="nectar-gradient-text" data-direction="horizontal" data-color="extra-color-gradient-1"
                             style="margin-top: -40px; "><h6>/mes</h6></div>
                        <div class="wpb_text_column wpb_content_element  vc_custom_1612463045732">
                          <div class="wpb_wrapper">
                            <p>Velocitats fins a x Mbps</p>
                          </div>
                        </div>

                        <div class="divider-wrap" data-alignment="default">
                          <div style="height: 25px;" class="divider"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </label></div>
            </div>
            <div id="grupofibra2" class="grupofibra">

              <div class="col selectwithhtml">
                <label>
                  <input type="radio" name="fibra" value="tres" class="hideradio">
                  <div
                    class="vc_col-sm-4 wpb_column column_container vc_column_container col boxed centered-text has-animation padding-4-percent animated-in instance-11"
                    data-t-w-inherits="default" data-shadow="none" data-border-radius="none" data-border-animation=""
                    data-border-animation-delay="" data-border-width="none" data-border-style="solid" data-border-color=""
                    data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1"
                    data-hover-bg="" data-hover-bg-opacity="1" data-animation="grow-in" data-delay="0"
                    style="transform: scale(1, 1); opacity: 1;">
                    <div class="column-bg-overlay"></div>
                    <div class="vc_column-inner">
                      <div class="wpb_wrapper">

                        <div class="wpb_text_column wpb_content_element ">
                          <div class="wpb_wrapper">
                            <h5>Internet Fibra Òptica 300</h5>
                          </div>
                        </div>


                        <div class="divider-wrap" data-alignment="default">
                          <div style="height: 25px;" class="divider"></div>
                        </div>
                        <div class="wpb_text_column wpb_content_element">
                          <div class="wpb_wrapper">
                            <p>Des de</p>
                          </div>
                        </div>


                        <div class="nectar-gradient-text precio" data-direction="horizontal" data-color="extra-color-gradient-1" style="">
                          <h2>30,00 €</h2></div>
                        <div class="nectar-gradient-text" data-direction="horizontal" data-color="extra-color-gradient-1"
                             style="margin-top: -40px; "><h6>/mes</h6></div>
                        <div class="wpb_text_column wpb_content_element">
                          <div class="wpb_wrapper">
                            <p>Velocitats fins a 300 Mbps</p>
                          </div>
                        </div>

                        <div class="divider-wrap" data-alignment="default">
                          <div style="height: 25px;" class="divider"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </label></div>
            </div>
            <div id="grupofibra3" class="grupofibra">

              <div class="col selectwithhtml"><label><input type="radio" name="fibra" value="cien" class="hideradio">
                  <div
                    class="vc_col-sm-4 wpb_column column_container vc_column_container col boxed centered-text has-animation padding-4-percent animated-in instance-12"
                    data-t-w-inherits="default" data-shadow="none" data-border-radius="none" data-border-animation=""
                    data-border-animation-delay="" data-border-width="none" data-border-style="solid" data-border-color=""
                    data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1"
                    data-hover-bg="" data-hover-bg-opacity="1" data-animation="grow-in" data-delay="0"
                    style="transform: scale(1, 1); opacity: 1;">
                    <div class="column-bg-overlay"></div>
                    <div class="vc_column-inner">
                      <div class="wpb_wrapper">

                        <div class="wpb_text_column wpb_content_element">
                          <div class="wpb_wrapper">
                            <h5>Internet Fibra Òptica 100</h5>
                          </div>
                        </div>


                        <div class="divider-wrap" data-alignment="default">
                          <div style="height: 25px;" class="divider"></div>
                        </div>
                        <div class="wpb_text_column wpb_content_element ">
                          <div class="wpb_wrapper">
                            <p>Des de</p>
                          </div>
                        </div>


                        <div class="nectar-gradient-text precio" data-direction="horizontal" data-color="extra-color-gradient-1" style="">
                          <h2>30,00 €</h2></div>
                        <div class="nectar-gradient-text" data-direction="horizontal" data-color="extra-color-gradient-1"
                             style="margin-top: -40px; "><h6>/mes</h6></div>
                        <div class="wpb_text_column wpb_content_element">
                          <div class="wpb_wrapper">
                            <p>Velocitats fins a 100 Mbps</p>
                          </div>
                        </div>

                        <div class="divider-wrap" data-alignment="default">
                          <div style="height: 25px;" class="divider"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </label><label><input type="radio" name="fibra" value="tres" class="hideradio">
                  <div
                    class="vc_col-sm-4 wpb_column column_container vc_column_container col boxed centered-text has-animation padding-4-percent animated-in instance-13"
                    data-t-w-inherits="default" data-shadow="none" data-border-radius="none" data-border-animation=""
                    data-border-animation-delay="" data-border-width="none" data-border-style="solid" data-border-color=""
                    data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1"
                    data-hover-bg="" data-hover-bg-opacity="1" data-animation="grow-in" data-delay="0"
                    style="transform: scale(1, 1); opacity: 1;">
                    <div class="column-bg-overlay"></div>
                    <div class="vc_column-inner">
                      <div class="wpb_wrapper">

                        <div class="wpb_text_column wpb_content_element">
                          <div class="wpb_wrapper">
                            <h5>Internet Fibra Òptica 300</h5>
                          </div>
                        </div>


                        <div class="divider-wrap" data-alignment="default">
                          <div style="height: 25px;" class="divider"></div>
                        </div>
                        <div class="wpb_text_column wpb_content_element">
                          <div class="wpb_wrapper">
                            <p>Des de</p>
                          </div>
                        </div>


                        <div class="nectar-gradient-text precio" data-direction="horizontal" data-color="extra-color-gradient-1" style="">
                          <h2>35,00 €</h2></div>
                        <div class="nectar-gradient-text" data-direction="horizontal" data-color="extra-color-gradient-1"
                             style="margin-top: -40px; "><h6>/mes</h6></div>
                        <div class="wpb_text_column wpb_content_element">
                          <div class="wpb_wrapper">
                            <p>Velocitats fins a 300 Mbps</p>
                          </div>
                        </div>

                        <div class="divider-wrap" data-alignment="default">
                          <div style="height: 25px;" class="divider"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </label></div>
            </div>
            <div id="grupofibra4" class="grupofibra">

              <div class="col selectwithhtml"><label><input type="radio" name="fibra" value="cien" class="hideradio">
                  <div
                    class="vc_col-sm-4 wpb_column column_container vc_column_container col boxed centered-text has-animation padding-4-percent animated-in instance-14"
                    data-t-w-inherits="default" data-shadow="none" data-border-radius="none" data-border-animation=""
                    data-border-animation-delay="" data-border-width="none" data-border-style="solid" data-border-color=""
                    data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1"
                    data-hover-bg="" data-hover-bg-opacity="1" data-animation="grow-in" data-delay="0"
                    style="transform: scale(1, 1); opacity: 1;">
                    <div class="column-bg-overlay"></div>
                    <div class="vc_column-inner">
                      <div class="wpb_wrapper">

                        <div class="wpb_text_column wpb_content_element ">
                          <div class="wpb_wrapper">
                            <h5>Internet Fibra Òptica 100</h5>
                          </div>
                        </div>


                        <div class="divider-wrap" data-alignment="default">
                          <div style="height: 25px;" class="divider"></div>
                        </div>
                        <div class="wpb_text_column wpb_content_element">
                          <div class="wpb_wrapper">
                            <p>Des de</p>
                          </div>
                        </div>


                        <div class="nectar-gradient-text precio" data-direction="horizontal" data-color="extra-color-gradient-1" style="">
                          <h2>30,00 €</h2></div>
                        <div class="nectar-gradient-text" data-direction="horizontal" data-color="extra-color-gradient-1"
                             style="margin-top: -40px; "><h6>/mes</h6></div>
                        <div class="wpb_text_column wpb_content_element ">
                          <div class="wpb_wrapper">
                            <p>Velocitats fins a 100 Mbps</p>
                          </div>
                        </div>

                        <div class="divider-wrap" data-alignment="default">
                          <div style="height: 25px;" class="divider"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </label><label><input type="radio" name="fibra" value="tres" class="hideradio">
                  <div
                    class="vc_col-sm-4 wpb_column column_container vc_column_container col boxed centered-text has-animation padding-4-percent animated-in instance-15"
                    data-t-w-inherits="default" data-shadow="none" data-border-radius="none" data-border-animation=""
                    data-border-animation-delay="" data-border-width="none" data-border-style="solid" data-border-color=""
                    data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1"
                    data-hover-bg="" data-hover-bg-opacity="1" data-animation="grow-in" data-delay="0"
                    style="transform: scale(1, 1); opacity: 1;">
                    <div class="column-bg-overlay"></div>
                    <div class="vc_column-inner">
                      <div class="wpb_wrapper">

                        <div class="wpb_text_column wpb_content_element ">
                          <div class="wpb_wrapper">
                            <h5>Internet Fibra Òptica 300</h5>
                          </div>
                        </div>


                        <div class="divider-wrap" data-alignment="default">
                          <div style="height: 25px;" class="divider"></div>
                        </div>
                        <div class="wpb_text_column wpb_content_element ">
                          <div class="wpb_wrapper">
                            <p>Des de</p>
                          </div>
                        </div>


                        <div class="nectar-gradient-text precio" data-direction="horizontal" data-color="extra-color-gradient-1" style="">
                          <h2>35,00 €</h2></div>
                        <div class="nectar-gradient-text" data-direction="horizontal" data-color="extra-color-gradient-1"
                             style="margin-top: -40px; "><h6>/mes</h6></div>
                        <div class="wpb_text_column wpb_content_element ">
                          <div class="wpb_wrapper">
                            <p>Velocitats fins a 300 Mbps</p>
                          </div>
                        </div>

                        <div class="divider-wrap" data-alignment="default">
                          <div style="height: 25px;" class="divider"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </label><label><input type="radio" name="fibra" value="rdoce" class="hideradio">
                  <div
                    class="vc_col-sm-4 wpb_column column_container vc_column_container col boxed centered-text has-animation padding-4-percent animated-in instance-16"
                    data-t-w-inherits="default" data-shadow="none" data-border-radius="none" data-border-animation=""
                    data-border-animation-delay="" data-border-width="none" data-border-style="solid" data-border-color=""
                    data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1"
                    data-hover-bg="" data-hover-bg-opacity="1" data-animation="grow-in" data-delay="0"
                    style="transform: scale(1, 1); opacity: 1;">
                    <div class="column-bg-overlay"></div>
                    <div class="vc_column-inner">
                      <div class="wpb_wrapper">

                        <div class="wpb_text_column wpb_content_element ">
                          <div class="wpb_wrapper">
                            <h5>Internet Rural</h5>
                          </div>
                        </div>


                        <div class="divider-wrap" data-alignment="default">
                          <div style="height: 25px;" class="divider"></div>
                        </div>
                        <div class="wpb_text_column wpb_content_element ">
                          <div class="wpb_wrapper">
                            <p>Des de</p>
                          </div>
                        </div>


                        <div class="nectar-gradient-text precio" data-direction="horizontal" data-color="extra-color-gradient-1" style="">
                          <h2>24,95 €</h2></div>
                        <div class="nectar-gradient-text" data-direction="horizontal" data-color="extra-color-gradient-1"
                             style="margin-top: -40px; "><h6>/mes</h6></div>
                        <div class="wpb_text_column wpb_content_element ">
                          <div class="wpb_wrapper">
                            <p>Velocitats fins a x Mbps</p>
                          </div>
                        </div>

                        <div class="divider-wrap" data-alignment="default">
                          <div style="height: 25px;" class="divider"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </label><label><input type="radio" name="fibra" value="rtreintaydos" class="hideradio">
                  <div
                    class="vc_col-sm-4 wpb_column column_container vc_column_container col boxed centered-text has-animation padding-4-percent animated-in instance-17"
                    data-t-w-inherits="default" data-shadow="none" data-border-radius="none" data-border-animation=""
                    data-border-animation-delay="" data-border-width="none" data-border-style="solid" data-border-color=""
                    data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1"
                    data-hover-bg="" data-hover-bg-opacity="1" data-animation="grow-in" data-delay="0"
                    style="transform: scale(1, 1); opacity: 1;">
                    <div class="column-bg-overlay"></div>
                    <div class="vc_column-inner">
                      <div class="wpb_wrapper">

                        <div class="wpb_text_column wpb_content_element ">
                          <div class="wpb_wrapper">
                            <h5>Internet Rural</h5>
                          </div>
                        </div>


                        <div class="divider-wrap" data-alignment="default">
                          <div style="height: 25px;" class="divider"></div>
                        </div>
                        <div class="wpb_text_column wpb_content_element ">
                          <div class="wpb_wrapper">
                            <p>Des de</p>
                          </div>
                        </div>


                        <div class="nectar-gradient-text precio" data-direction="horizontal" data-color="extra-color-gradient-1" style="">
                          <h2>35,95 €</h2></div>
                        <div class="nectar-gradient-text" data-direction="horizontal" data-color="extra-color-gradient-1"
                             style="margin-top: -40px; "><h6>/mes</h6></div>
                        <div class="wpb_text_column wpb_content_element ">
                          <div class="wpb_wrapper">
                            <p>Velocitats fins a x Mbps</p>
                          </div>
                        </div>

                        <div class="divider-wrap" data-alignment="default">
                          <div style="height: 25px;" class="divider"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </label><label><input type="radio" name="fibra" value="rcincuenta" class="hideradio">
                  <div
                    class="vc_col-sm-4 wpb_column column_container vc_column_container col boxed centered-text has-animation padding-4-percent animated-in instance-18"
                    data-t-w-inherits="default" data-shadow="none" data-border-radius="none" data-border-animation=""
                    data-border-animation-delay="" data-border-width="none" data-border-style="solid" data-border-color=""
                    data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1"
                    data-hover-bg="" data-hover-bg-opacity="1" data-animation="grow-in" data-delay="0"
                    style="transform: scale(1, 1); opacity: 1;">
                    <div class="column-bg-overlay"></div>
                    <div class="vc_column-inner">
                      <div class="wpb_wrapper">

                        <div class="wpb_text_column wpb_content_element ">
                          <div class="wpb_wrapper">
                            <h5>Internet Rural</h5>
                          </div>
                        </div>


                        <div class="divider-wrap" data-alignment="default">
                          <div style="height: 25px;" class="divider"></div>
                        </div>
                        <div class="wpb_text_column wpb_content_element ">
                          <div class="wpb_wrapper">
                            <p>Des de</p>
                          </div>
                        </div>


                        <div class="nectar-gradient-text precio" data-direction="horizontal" data-color="extra-color-gradient-1" style="">
                          <h2>54,95 €</h2></div>
                        <div class="nectar-gradient-text" data-direction="horizontal" data-color="extra-color-gradient-1"
                             style="margin-top: -40px; "><h6>/mes</h6></div>
                        <div class="wpb_text_column wpb_content_element ">
                          <div class="wpb_wrapper">
                            <p>Velocitats fins a x Mbps</p>
                          </div>
                        </div>

                        <div class="divider-wrap" data-alignment="default">
                          <div style="height: 25px;" class="divider"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </label></div>
            </div>
            <div id="grupofibra5" class="grupofibra">

              <div class="col selectwithhtml"><label><input type="radio" name="fibra" value="rdoce" class="hideradio">
                  <div
                    class="vc_col-sm-4 wpb_column column_container vc_column_container col boxed centered-text has-animation padding-4-percent animated-in instance-19"
                    data-t-w-inherits="default" data-shadow="none" data-border-radius="none" data-border-animation=""
                    data-border-animation-delay="" data-border-width="none" data-border-style="solid" data-border-color=""
                    data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1"
                    data-hover-bg="" data-hover-bg-opacity="1" data-animation="grow-in" data-delay="0"
                    style="transform: scale(1, 1); opacity: 1;">
                    <div class="column-bg-overlay"></div>
                    <div class="vc_column-inner">
                      <div class="wpb_wrapper">

                        <div class="wpb_text_column wpb_content_element ">
                          <div class="wpb_wrapper">
                            <h5>Internet Rural</h5>
                          </div>
                        </div>


                        <div class="divider-wrap" data-alignment="default">
                          <div style="height: 25px;" class="divider"></div>
                        </div>
                        <div class="wpb_text_column wpb_content_element  ">
                          <div class="wpb_wrapper">
                            <p>Des de</p>
                          </div>
                        </div>


                        <div class="nectar-gradient-text precio" data-direction="horizontal" data-color="extra-color-gradient-1" style="">
                          <h2>24,95 €</h2></div>
                        <div class="nectar-gradient-text" data-direction="horizontal" data-color="extra-color-gradient-1"
                             style="margin-top: -40px; "><h6>/mes</h6></div>
                        <div class="wpb_text_column wpb_content_element ">
                          <div class="wpb_wrapper">
                            <p>Velocitats fins a x Mbps</p>
                          </div>
                        </div>

                        <div class="divider-wrap" data-alignment="default">
                          <div style="height: 25px;" class="divider"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </label><label><input type="radio" name="fibra" value="rtreintaydos" class="hideradio">
                  <div
                    class="vc_col-sm-4 wpb_column column_container vc_column_container col boxed centered-text has-animation padding-4-percent animated-in instance-20"
                    data-t-w-inherits="default" data-shadow="none" data-border-radius="none" data-border-animation=""
                    data-border-animation-delay="" data-border-width="none" data-border-style="solid" data-border-color=""
                    data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1"
                    data-hover-bg="" data-hover-bg-opacity="1" data-animation="grow-in" data-delay="0"
                    style="transform: scale(1, 1); opacity: 1;">
                    <div class="column-bg-overlay"></div>
                    <div class="vc_column-inner">
                      <div class="wpb_wrapper">

                        <div class="wpb_text_column wpb_content_element ">
                          <div class="wpb_wrapper">
                            <h5>Internet Rural</h5>
                          </div>
                        </div>


                        <div class="divider-wrap" data-alignment="default">
                          <div style="height: 25px;" class="divider"></div>
                        </div>
                        <div class="wpb_text_column wpb_content_element  vc_custom_1587921730419">
                          <div class="wpb_wrapper">
                            <p>Des de</p>
                          </div>
                        </div>


                        <div class="nectar-gradient-text precio" data-direction="horizontal" data-color="extra-color-gradient-1" style="">
                          <h2>35,95 €</h2></div>
                        <div class="nectar-gradient-text" data-direction="horizontal" data-color="extra-color-gradient-1"
                             style="margin-top: -40px; "><h6>/mes</h6></div>
                        <div class="wpb_text_column wpb_content_element  vc_custom_1612463045732">
                          <div class="wpb_wrapper">
                            <p>Velocitats fins a x Mbps</p>
                          </div>
                        </div>

                        <div class="divider-wrap" data-alignment="default">
                          <div style="height: 25px;" class="divider"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </label><label><input type="radio" name="fibra" value="rcincuenta" class="hideradio">
                  <div
                    class="vc_col-sm-4 wpb_column column_container vc_column_container col boxed centered-text has-animation padding-4-percent animated-in instance-21"
                    data-t-w-inherits="default" data-shadow="none" data-border-radius="none" data-border-animation=""
                    data-border-animation-delay="" data-border-width="none" data-border-style="solid" data-border-color=""
                    data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1"
                    data-hover-bg="" data-hover-bg-opacity="1" data-animation="grow-in" data-delay="0"
                    style="transform: scale(1, 1); opacity: 1;">
                    <div class="column-bg-overlay"></div>
                    <div class="vc_column-inner">
                      <div class="wpb_wrapper">

                        <div class="wpb_text_column wpb_content_element ">
                          <div class="wpb_wrapper">
                            <h5>Internet Rural</h5>
                          </div>
                        </div>


                        <div class="divider-wrap" data-alignment="default">
                          <div style="height: 25px;" class="divider"></div>
                        </div>
                        <div class="wpb_text_column wpb_content_element  vc_custom_1587921730419">
                          <div class="wpb_wrapper">
                            <p>Des de</p>
                          </div>
                        </div>


                        <div class="nectar-gradient-text precio" data-direction="horizontal" data-color="extra-color-gradient-1" style="">
                          <h2>54,95 €</h2></div>
                        <div class="nectar-gradient-text" data-direction="horizontal" data-color="extra-color-gradient-1"
                             style="margin-top: -40px; "><h6>/mes</h6></div>
                        <div class="wpb_text_column wpb_content_element  vc_custom_1612463045732">
                          <div class="wpb_wrapper">
                            <p>Velocitats fins a x Mbps</p>
                          </div>
                        </div>

                        <div class="divider-wrap" data-alignment="default">
                          <div style="height: 25px;" class="divider"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </label></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- --------------------------- Telefonía fija ------------------------------ -->
    <div class="accordion-tel" id="accordion">
      <div class="panel-accordion">
        <a data-toggle="collapse" data-parent="#accordion" data-target="#collapse1"
           aria-expanded="true" id="expanded-tel" data-popover="Debe seleccionar una tarifa">
          <div class="panel-heading card-header">
            <h4 class="panel-title"><label for="00" class="tn-headline">
                <div id="caja"><img
                src="<?php echo plugins_url().'/wp-configurador/ico/icons8-telefono-50.png'?>" alt="Sikarra telefono">
                  <p class="card-title-catg">TELEFONÍA FIJA</p></div>
              </label></h4>
            <span style="float:right; top:0rem; margin-top:-47px;"><input class="input-acordeon" name="conf[]"  type="checkbox" id="switch2" required><label class="label-acordeon" for="switch2">Toggle</label></span>
          </div>
        </a>
        <div class="collapse" id="collapse1">
          <div class="card card-body">
            
            <div id="grupofijo1" class="grupofijo">
              <div class="col selectwithhtml"><label><input type="radio" name="fijo" value="quinientos" class="hideradio">
                  <div
                    class="vc_col-sm-4 wpb_column column_container vc_column_container col boxed centered-text has-animation padding-4-percent animated-in instance-22"
                    data-t-w-inherits="default" data-shadow="none" data-border-radius="none" data-border-animation=""
                    data-border-animation-delay="" data-border-width="none" data-border-style="solid" data-border-color=""
                    data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1"
                    data-hover-bg="" data-hover-bg-opacity="1" data-animation="grow-in" data-delay="0"
                    style="transform: scale(1, 1); opacity: 1;">
                    <div class="column-bg-overlay"></div>
                    <div class="vc_column-inner">
                      <div class="wpb_wrapper">

                        <div class="wpb_text_column wpb_content_element ">
                          <div class="wpb_wrapper">
                            <h5>Fixa 500</h5>
                          </div>
                        </div>


                        <div class="divider-wrap" data-alignment="default">
                          <div style="height: 25px;" class="divider"></div>
                        </div>
                        <div class="wpb_text_column wpb_content_element">
                          <div class="wpb_wrapper">
                            <p>Des de</p>
                          </div>
                        </div>


                        <div class="nectar-gradient-text precio" data-direction="horizontal" data-color="extra-color-gradient-1" style="">
                          <h2>5,00 €</h2></div>
                        <div class="nectar-gradient-text" data-direction="horizontal" data-color="extra-color-gradient-1"
                             style="margin-top: -40px; "><h6>/mes IVA inclòs</h6></div>
                        <div class="wpb_text_column wpb_content_element ">
                          <div class="wpb_wrapper">
                            <p>500 minuts + 100 mòbils</p>
                          </div>
                        </div>

                        <div class="divider-wrap" data-alignment="default">
                          <div style="height: 25px;" class="divider"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </label><label><input type="radio" name="fijo" value="dosmil" class="hideradio">
                  <div
                    class="vc_col-sm-4 wpb_column column_container vc_column_container col boxed centered-text has-animation padding-4-percent animated-in instance-23"
                    data-t-w-inherits="default" data-shadow="none" data-border-radius="none" data-border-animation=""
                    data-border-animation-delay="" data-border-width="none" data-border-style="solid" data-border-color=""
                    data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1"
                    data-hover-bg="" data-hover-bg-opacity="1" data-animation="grow-in" data-delay="0"
                    style="transform: scale(1, 1); opacity: 1;">
                    <div class="column-bg-overlay"></div>
                    <div class="vc_column-inner">
                      <div class="wpb_wrapper">

                        <div class="wpb_text_column wpb_content_element ">
                          <div class="wpb_wrapper">
                            <h5>Fixa 2000</h5>
                          </div>
                        </div>


                        <div class="divider-wrap" data-alignment="default">
                          <div style="height: 25px;" class="divider"></div>
                        </div>
                        <div class="wpb_text_column wpb_content_element  ">
                          <div class="wpb_wrapper">
                            <p>Des de</p>
                          </div>
                        </div>


                        <div class="nectar-gradient-text precio" data-direction="horizontal" data-color="extra-color-gradient-1" style="">
                          <h2>9,00 €</h2></div>
                        <div class="nectar-gradient-text" data-direction="horizontal" data-color="extra-color-gradient-1"
                             style="margin-top: -40px; "><h6>/mes IVA inclòs</h6></div>
                        <div class="wpb_text_column wpb_content_element ">
                          <div class="wpb_wrapper">
                            <p>2000 minuts + 500 mòbils</p>
                          </div>
                        </div>

                        <div class="divider-wrap" data-alignment="default">
                          <div style="height: 25px;" class="divider"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </label></div>
            </div>
            <div id="grupofijo2" class="grupofijo">
              <div class="col selectwithhtml"><label><input type="radio" name="fijo" value="quinientos" class="hideradio">
                  <div
                    class="vc_col-sm-4 wpb_column column_container vc_column_container col boxed centered-text has-animation padding-4-percent animated-in instance-24"
                    data-t-w-inherits="default" data-shadow="none" data-border-radius="none" data-border-animation=""
                    data-border-animation-delay="" data-border-width="none" data-border-style="solid" data-border-color=""
                    data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1"
                    data-hover-bg="" data-hover-bg-opacity="1" data-animation="grow-in" data-delay="0"
                    style="transform: scale(1, 1); opacity: 1;">
                    <div class="column-bg-overlay"></div>
                    <div class="vc_column-inner">
                      <div class="wpb_wrapper">

                        <div class="wpb_text_column wpb_content_element ">
                          <div class="wpb_wrapper">
                            <h5>Fixa 500</h5>
                          </div>
                        </div>


                        <div class="divider-wrap" data-alignment="default">
                          <div style="height: 25px;" class="divider"></div>
                        </div>
                        <div class="wpb_text_column wpb_content_element  ">
                          <div class="wpb_wrapper">
                            <p>Des de</p>
                          </div>
                        </div>


                        <div class="nectar-gradient-text precio" data-direction="horizontal" data-color="extra-color-gradient-1" style="">
                          <h2>4,95 €</h2></div>
                        <div class="nectar-gradient-text" data-direction="horizontal" data-color="extra-color-gradient-1"
                             style="margin-top: -40px; "><h6>/mes IVA inclòs</h6></div>
                        <div class="wpb_text_column wpb_content_element ">
                          <div class="wpb_wrapper">
                            <p>500 minuts + 100 mòbils</p>
                          </div>
                        </div>

                        <div class="divider-wrap" data-alignment="default">
                          <div style="height: 25px;" class="divider"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </label><label><input type="radio" name="fijo" value="dosmil" class="hideradio">
                  <div
                    class="vc_col-sm-4 wpb_column column_container vc_column_container col boxed centered-text has-animation padding-4-percent animated-in instance-25"
                    data-t-w-inherits="default" data-shadow="none" data-border-radius="none" data-border-animation=""
                                                  data-border-animation-delay="" data-border-width="none" data-border-style="solid" data-border-color=""
                    data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1"
                    data-hover-bg="" data-hover-bg-opacity="1" data-animation="grow-in" data-delay="0"
                    style="transform: scale(1, 1); opacity: 1;">
                    <div class="column-bg-overlay"></div>
                    <div class="vc_column-inner">
                      <div class="wpb_wrapper">

                        <div class="wpb_text_column wpb_content_element ">
                          <div class="wpb_wrapper">
                            <h5>Fixa 2000</h5>
                          </div>
                        </div>


                        <div class="divider-wrap" data-alignment="default">
                          <div style="height: 25px;" class="divider"></div>
                        </div>
                        <div class="wpb_text_column wpb_content_element">
                          <div class="wpb_wrapper">
                            <p>Des de</p>
                          </div>
                        </div>


                        <div class="nectar-gradient-text precio" data-direction="horizontal" data-color="extra-color-gradient-1" style="">
                          <h2>9,00 €</h2></div>
                        <div class="nectar-gradient-text" data-direction="horizontal" data-color="extra-color-gradient-1"
                             style="margin-top: -40px; "><h6>/mes IVA inclòs</h6></div>
                        <div class="wpb_text_column wpb_content_element">
                          <div class="wpb_wrapper">
                            <p>2000 minuts + 500 mòbils</p>
                          </div>
                        </div>

                        <div class="divider-wrap" data-alignment="default">
                          <div style="height: 25px;" class="divider"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </label></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- --------------------------- Móvil ------------------------------ -->
    <div class="accordion-movil" id="accordion">
      <div class="panel-accordion">
        <a data-toggle="collapse" data-parent="#accordion" data-target="#collapse1"
           aria-expanded="true" id="expanded-movil">
          <div class="panel-heading card-header">
            <h4 class="panel-title"><label for="00" class="tn-headline">
                <div id="caja"><img
                src="<?php echo plugins_url().'/wp-configurador/ico/Line-05.png'?>" alt="Sikarra movil">
                  <p class="card-title-catg">MÓVIL</p></div>
              </label></h4>
            <span style="float:right; top:0rem; margin-top:-47px;"><input class="input-acordeon" name="conf[]" type="checkbox" id="switch3" required><label class="label-acordeon" for="switch3">Toggle</label></span>
          </div>
        </a>
        <div class="collapse" id="collapse1">
          <div class="card card-body">
            <div class="row-slider">
              <div class="slider-tipos">
                <label>Minuts</label>
                <div class="cont-slider">
                  <div class="slider minutos" name="minutos"></div>
                  <div class="noUi-pips noUi-pips-horizontal steps-slider">
                    <div class="noUi-marker noUi-marker-horizontal noUi-marker-large" style="left: 0%;"></div>
                    <div class="noUi-value noUi-value-horizontal noUi-value-large" data-value="0" style="left: 0%;">50</div>
                    <div class="noUi-marker noUi-marker-horizontal noUi-marker-large" style="left: 50%;"></div>
                    <div class="noUi-value noUi-value-horizontal noUi-value-large" data-value="1" style="left: 50%;">300</div>
                    <div class="noUi-marker noUi-marker-horizontal noUi-marker-large" style="left: 100%;"></div>
                    <div class="noUi-value noUi-value-horizontal noUi-value-large" data-value="2" style="left: 100%;">IL·LIMITATS</div>
                  </div>
                  <input type="hidden" name="minutos" value="0">
                </div>
              </div>
              <div class="slider-tipos">
                <label>GB</label>
                <div class="cont-slider">
                  <div class="slider gigas" name="gigas"></div>
                  <input type="hidden" name="gigas" value="3">
                </div>
              </div>
            </div>
            <div id="grupomovil" class="grupomovil">
              <div class="col selectmovil">
                <div
                  class="vc_col-sm-3 wpb_column column_container vc_column_container col boxed centered-text no-extra-padding instance-2 one-fourths clear-both smovil"
                  data-t-w-inherits="default" data-shadow="none" data-border-radius="none" data-border-animation=""
                  data-border-animation-delay="" data-border-width="none" data-border-style="solid" data-border-color=""
                  data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1"
                  data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
                  <input type="checkbox" id="toggle1"/>
                  <label class="button" for="toggle1" data-bs-toggle="popover"></label>
                  <div class="column-bg-overlay"></div>
                  <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                      <div class="wpb_text_column wpb_content_element ">
                        <div class="wpb_wrapper minutos">
                          <h5>50 minuts</h5>
                        </div>
                      </div>
                      <div class="nectar-gradient-text precio" data-direction="horizontal" data-color="extra-color-gradient-1"
                           style=""><h2><span>8</span>,75 €</h2></div>
                      <div class="nectar-gradient-text" data-direction="horizontal" data-color="extra-color-gradient-1"
                           style="margin-top: -40px; "><h6>/mes IVA inclòs</h6></div>
                      <div class="wpb_text_column wpb_content_element  vc_custom_1612463045732">
                        <div class="wpb_wrapper datos">
                          <p><span>5</span> GB de dades</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- --------------------------- fin ------------------------------ -->
      <div class="popover-tooltip1 tooltip" role="tooltip">Debes seleccionar una tarifa de internet
        <div class="popover-arrow" data-popper-arrow></div>
      </div>
      <div class="popover-tooltip2 tooltip" role="tooltip">Has llegado al limite permitido
        <div class="popover-arrow" data-popper-arrow></div>
      </div>
    </div>
    <input type="button" name="previous" class="nectar-button jumbo regular accent-color  regular-button" data-color-override="false" value="Anterior" />
    <input type="submit" name="next" class="nectar-button jumbo regular accent-color  regular-button siguiente" data-color-override="false" value="Siguiente"/>
    </form>
  </div>

</fieldset>