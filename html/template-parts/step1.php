<?php
/**
 * Created by IntelliJ IDEA.
 * User: Luis Calero, Juan Carlos Muruaga Ceballos.
 * Date: 04/10/2018
 * Time: 11:01
 */
?>
<div class="navbottom">
  <div class="internet"></div>
  <div class="telefonia"></div>
  <div class="movil"></div>
  <div class="total">
    <div class="vc_col-sm-1 col boxed centered-text popever-internet">
      <div class="vc_column-inner">
        <div class="wpb_wrapper">
          <div class="wpb_text_column wpb_content_element ">
            <div class="wpb_wrapper">
              <h5>TOTAL</h5>
            </div>
          </div>
          <div class="nectar-gradient-text preciototal" data-direction="horizontal" data-color="extra-color-gradient-1" style="">
            <h5><span></span> €</h5>
          </div>
          <div class="nectar-gradient-text" data-direction="horizontal" data-color="extra-color-gradient-1" style="margin-top: -40px; ">
            <h6>/mes</h6>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<fieldset>
  <div class="panel-localidad">
    <h2 style="font-size: 20px;text-align: center" class="vc_custom_heading">Seleccione una localidad</h2>
    <form action="javascript:0">
      <div>
        <div class="box-texto">
            <input type="radio" id="prats" class="regular-radio" name="localidad" value="Els Prats de Rei" required>
            <label class="titulo-label" for="prats">Els Prats de Rei</label>
        </div>
      </div>
      <div>
          <div class="box-texto">
              <input type="radio" id="calaf" class="regular-radio" name="localidad" value="Calaf" required>
              <label class="titulo-label" for="calaf">Calaf</label>
          </div>
      </div>
      <div>
          <div class="box-texto">
              <input type="radio" id="marti" class="regular-radio" name="localidad" value="Sant Martí Sesgueioles" required>
              <label class="titulo-label" for="marti">Sant Martí Sesgueioles</label>
          </div>
      </div>
      <div>
          <div class="box-texto">
              <input type="radio" id="pere" class="regular-radio" name="localidad" value="Sant Pere Sallavinera" required>
              <label class="titulo-label" for="pere">Sant Pere Sallavinera</label>
          </div>
      </div>
      <div>
          <div class="box-texto">
              <input type="radio" id="veciana" class="regular-radio" name="localidad" value="Veciana" required>
              <label class="titulo-label" for="veciana">Veciana</label>
          </div>
      </div>
      <div class="box-button">
        <input type="submit" name="next" class="nectar-button jumbo regular accent-color regular-button configurador" data-color-override="false" value="CONFIGURAR TARIFA" style="padding: 25px 40px !important;"/>
      </div>
    </form>
  </div>
</fieldset>

