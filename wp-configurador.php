<?php


/**
 *
 * Plugin Name: wp-configurador
 * Plugin URL: no url
 * Description: Configurador de tarifas
 * Version: 1.0.0
 * Author: Luis Calero
 *
 * */

function wp_configurador ($atts)
{
  include 'html/template-parts/step1.php';
  include 'html/template-parts/step2.php';
  include 'html/template-parts/step3.php';
}
/**
 * CSS & JS.
 */
function configurador_enqueue_scripts()
{
  // jquery
  wp_enqueue_style( 'wp-jquery-ui-conf', plugins_url('/wp-configurador/libs/jquery-ui/css/nouislider.min.css'), __FILE__);
  wp_enqueue_script( 'wp-jquery-ui-conf', plugins_url('/wp-configurador/libs/jquery-ui/js/nouislider.min.js'), array( 'jquery' ), '1.0.0', true  );
  wp_enqueue_script( 'popover-conf', plugins_url('/wp-configurador/libs/popover-main/popper.min.js'), array( 'jquery' ), '1.0.3', true  );
  wp_enqueue_script('wp-script-conf', plugins_url('/wp-configurador/js/configurador.js'), array( 'jquery' ), '1.1.7', true);
  if (locate_template('configurador.css') != '') {
    wp_enqueue_style('wp-style-conf', get_stylesheet_directory_uri() . '/wp-configurador/css/configurador.css', __FILE__);
  } else {
    wp_enqueue_style('wp-style-conf', plugins_url('/wp-configurador/css/configurador.css'), __FILE__);
  }
}

add_action( 'wp_enqueue_scripts', 'configurador_enqueue_scripts' );



/*
function requires() {
  require_once 'calculadora-common.php';
  require_once 'calculadora-config.php';
  require_once 'data/DataFinanciacion.php';

  require_once 'ajax/calculadora-ajax.php';
}
requires();
*/


// Tag del plugin.
add_shortcode('wp_configurador', 'wp_configurador');