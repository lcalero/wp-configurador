(function ($) {
  $(document).ready(function () {
    datosGigas = [
      {valores: [5, 12]},
      {valores: [3, 6]},
      {valores: [0, 3, 12, 17, 22, 32, 52, 92]}
    ];
    datosMinutos = ['50 minuts','300 minuts','il·limitades'];
    datosPrecio = [[8,9],[8,9],[8,9,11,12,15,16,19,20]];

    var elemento = $('.slider.minutos')[0],
      elemento2 = $('.slider.gigas')[0];

    noUiSlider.create(elemento, {
      start: 0,
      connect: 'lower',
      step: 1,
      range: {
        'min': 0,
        'max': 2
      }
    });
    noUiSlider.create(elemento2, {
      start: 0,
      connect: 'lower',
      snap: true,
      range: {
        'min': 5,
        'max': 12
      },
      pips: {
        mode: 'values',
        values: [5,12],
        density: 100
      }
    });
    /* Crear un rango homogeneo en porcentajes */
    calcularRango = function(datos){
      var rango = {'min': datos[0]},
        porcentaje = 100 / (datos.length-1),
        acumulador = 0;
      datos.forEach(function(element,index){
        if(index > 0 && index < datos.length-1){
          acumulador+= porcentaje;
          rango[acumulador+'%'] = element;
        }
      });
      rango.max = datos[datos.length-1];
      return rango;
    };
    cambiarSliderGigas = function(index) {
      var valores = datosGigas[index].valores;

      elemento2.noUiSlider.updateOptions({
        start: [0],
        snap: true,
        range: calcularRango(valores),
        pips: {
          mode: 'values',
          values: valores,
          density: 100
        }
      });
    };

    cambioTarifa = function (minutos,gigas) {
      var index = datosGigas[parseInt(minutos)].valores.findIndex(function(item, i){
        return item === parseInt(gigas);
      });
      $('.grupomovil .smovil:last .minutos h5').html(datosMinutos[minutos]);
      $('.grupomovil .smovil:last .precio h2 span').html(datosPrecio[minutos][index]);
      $('.movil .popover-movil:last .precio h5 span').html(datosPrecio[minutos][index]);
      $('.grupomovil .smovil:last .datos p span').html(gigas);
    };
    elemento.noUiSlider.on('update', function (values) {
      cambiarSliderGigas(parseInt(values[0]));
      $(this.target).parent().find('input[name="minutos"]').val(parseInt(values[0]));
    });
    elemento.noUiSlider.on('change', function (values) {
      cambioTarifa(parseInt(values[0]), parseInt($('input[name="gigas"]').val()));
      sumaTotal();
    });
    elemento2.noUiSlider.on('update', function (values) {
      $(this.target).parent().find('input[name="gigas"]').val(parseInt(values[0]));
    });
    elemento2.noUiSlider.on('change', function (values) {
      cambioTarifa(parseInt($('input[name="minutos"]').val()), parseInt(values[0]));
      sumaTotal();
    });
    $('input.regular-radio').change(function (){
      $('.grupofibra').removeClass('active');
      switch ($(this).val()) {
        case "Els Prats de Rei":
          $('#grupofibra1').addClass('active');
          break;
        case "Calaf":
          $('#grupofibra2').addClass('active');
          break;
        case "Sant Martí Sesgueioles":
          $('#grupofibra3').addClass('active');
          break;
        case "Sant Pere Sallavinera":
          $('#grupofibra4').addClass('active');
          break;
        case "Veciana":
          $('#grupofibra5').addClass('active');
          break;
      }
    });
    var inputFibra = $('input[name="fibra"]');
    inputFibra.change(function (){
      $('.grupofijo').removeClass('active');
      var grupo1 = $('#grupofijo1');
      switch ($(this).val()) {
        case "cien":
          grupo1.addClass('active');
          break;
        case "tres":
          grupo1.addClass('active');
          break;
        default:
          $('#grupofijo2').addClass('active');
          break;
      }
    });
    const button = document.querySelector('#expanded-internet');
    const tooltip = document.querySelector('.popover-tooltip1');
    const button2 = document.querySelector('.grupomovil');
    const tooltip2 = document.querySelector('.popover-tooltip2');
    const tooltipAll = $('.tooltip');

    const popperInstance1 = Popper.createPopper(button, tooltip, {
      placement: 'bottom',
      modifiers: [
        {
          name: 'offset',
          options: {
            offset: [0, 8]
          }
        }
      ]
    });
    function show(event, tooltip, popperInstance) {
      event.stopImmediatePropagation();
      tooltip.setAttribute('data-show', '');
      popperInstance.update();
    }
    function hide() {
      tooltipAll.each(function(){
        $(this)[0].removeAttribute('data-show');
      });
    }
    $('body').click(function () {
      hide();
    });
    var content= $('.navbottom');
    function agregarMovil(precio, id){
      content.find('.movil').append('<div class="vc_col-sm-1 col boxed centered-text popover-movil '+id+'">\n' +
        '      <div class="vc_column-inner">\n' +
        '        <div class="wpb_wrapper">\n' +
        '          <div class="wpb_text_column wpb_content_element ">\n' +
        '            <div class="wpb_wrapper">\n' +
        '              <h5>Móvil</h5>\n' +
        '            </div>\n' +
        '          </div>\n' +
        '          <div class="nectar-gradient-text precio" data-direction="horizontal" data-color="extra-color-gradient-1" style="">\n' +
        '            <h5><span>'+precio+'</span>,75 €</h5></div>\n' +
        '          <div class="nectar-gradient-text" data-direction="horizontal" data-color="extra-color-gradient-1" style="margin-top: -40px; "><h6>/mes</h6></div>\n' +
        '        </div>\n' +
        '      </div>\n' +
        '    </div>');
    }
    $('[id^="expanded-"]').click(function (event){
      var fieldcheck = $(this).parents('.panel-accordion').find('.input-acordeon');
      var expandAll = $(this).attr('id');
      if (fieldcheck.is(':checked')) {
        fieldcheck.prop("checked", false);
        $(this).parents('.panel-accordion').find('.collapse').removeClass('show');
        if(expandAll === "expanded-internet"){
          $('input[name="fibra"],#switch2,input[name="fijo"]').prop('checked', false);
          $('#expanded-tel').parents('.panel-accordion').find('.collapse').removeClass('show');
          if(!content.find('.popover-movil').length){
            content.removeClass('active');
            content.find('.internet,.telefonia').empty();
            content.find('.movil').addClass('disabled');
          }else{
            content.find('.internet,.telefonia').empty();
          }
          $('.input-acordeon').prop("required", true);
        }
        else if((expandAll === "expanded-movil")){
          $('.input-acordeon').prop("required", true);
          if(!content.find('.popover-internet').length){
            content.removeClass('active');
            content.find('.internet,.telefonia').empty();
            content.find('.movil').addClass('disabled');
          }else{
            content.find('.movil').addClass('disabled');
          }
        }else if((expandAll === "expanded-tel")){
          content.find('.telefonia').empty();
          $('input[name="fijo"]').prop('checked', false);
        }
      } else {
        if((expandAll === "expanded-tel") && (!$('input[name="fibra"]').is(':checked'))) {
          setTimeout(function() {
            show(event, tooltip, popperInstance1);
          }, 100);
          $("html, body").animate({
            scrollTop: 200
          }, 700);
          event.preventDefault();
          return false;
        }else{
          //console.log(fieldcheck[0]);
          fieldcheck.prop('checked', true);
          //$(this).parent().attr('aria-expanded', false);
          //$(this).parent().addClass('collapsed');
          $(this).parents('.panel-accordion').find('.collapse').addClass('show');
          if((expandAll === "expanded-movil")){
            var precio = $(this).parent().find('.precio span').text();
            $('.input-acordeon').prop("required", false);
            if(!content.hasClass('active')){
              content.addClass('active');
            }
            if( content.find('.movil').hasClass('disabled')){
              content.find('.movil').removeClass('disabled');
            }else{
              agregarMovil(precio,$(this).parent().find('.smovil input[type="checkbox"]').attr('id'));
            }
            sumaTotal();
          }
          return false;
        }
      }
    });
    inputFibra.click(function () {
      $('.input-acordeon').prop("required", false);
      if ($(this).is(':checked') && $('#switch1').is(':checked')){
        var precio = $(this).parent().find('.precio').text();

        if(!content.hasClass('active')){
          content.addClass('active');
        }
        if(!content.find('.popover-internet').length){
          content.find('.internet').append('<div class="vc_col-sm-1 col boxed centered-text popover-internet">\n' +
            '      <div class="vc_column-inner">\n' +
            '        <div class="wpb_wrapper">\n' +
            '          <div class="wpb_text_column wpb_content_element ">\n' +
            '            <div class="wpb_wrapper">\n' +
            '              <h5>Internet</h5>\n' +
            '            </div>\n' +
            '          </div>\n' +
            '          <div class="nectar-gradient-text precio" data-direction="horizontal" data-color="extra-color-gradient-1" style="">\n' +
            '            <h5>'+precio+'</h5></div>\n' +
            '          <div class="nectar-gradient-text" data-direction="horizontal" data-color="extra-color-gradient-1" style="margin-top: -40px; "><h6>/mes</h6></div>\n' +
            '        </div>\n' +
            '      </div>\n' +
            '    </div>');
        }else{
          content.find('.popover-internet .precio h5').text(precio);
        }
      }
      sumaTotal();
    });
    $('input[name="fijo"]').click(function(){
      var precio = $(this).parent().find('.precio').text();
      if(!content.find('.popover-telefonia').length){
        content.find('.telefonia').append('<div class="vc_col-sm-1 col boxed centered-text popover-telefonia">\n' +
          '      <div class="vc_column-inner">\n' +
          '        <div class="wpb_wrapper">\n' +
          '          <div class="wpb_text_column wpb_content_element ">\n' +
          '            <div class="wpb_wrapper">\n' +
          '              <h5>Teléfono</h5>\n' +
          '            </div>\n' +
          '          </div>\n' +
          '          <div class="nectar-gradient-text precio" data-direction="horizontal" data-color="extra-color-gradient-1" style="">\n' +
          '            <h5>'+precio+'</h5></div>\n' +
          '          <div class="nectar-gradient-text" data-direction="horizontal" data-color="extra-color-gradient-1" style="margin-top: -40px; "><h6>/mes</h6></div>\n' +
          '        </div>\n' +
          '      </div>\n' +
          '    </div>');
      }else{
        content.find('.popover-telefonia .precio h5').text(precio);
      }
      sumaTotal();
    });
    var sumaTotal = function () {
        var suma= 0;
        $('.navbottom:not(.movil.disabled) .precio h5').each(function() {
          var precio = $(this).text().split(" €");
          precio = $.trim(precio[0]);
          if(precio !== ''){
            precio = precio.replace(",", ".");
            precio = parseFloat(precio);
            suma += precio;
          }
        });
        suma = suma.toString().replace(".", ",");
        $('.total .preciototal span').html(suma);
    };
    var current = 1,current_step,next_step,steps;
    steps = $("fieldset").length;
    var tabs = $('.tab-configurador');
    $('input[name="next"]').click(function(e){
        var isvalidate = $(this).closest('form')[0].checkValidity();
        if($(this).hasClass('siguiente') && !$('#expanded-movil').is(':checked') && !$('input[name="fibra"]').is(':checked') && $('#expanded-internet').find('.input-acordeon').is(':checked')) {
          e.preventDefault();
          $('.input-acordeon').prop("required", true);
          show(e, tooltip, popperInstance1);
          $("html, body").animate({
            scrollTop: 200
          }, 700)
        }else{
          if (isvalidate) {
            current_step = $(this).parents('fieldset');
            next_step = current_step.next();
            next_step.show();
            current_step.hide();
            setProgressBar(++current);
            $('input.fibra-final').val($('input[name="fibra"]:checked').val());
            $('input.telefono-final').val($('input[name="fijo"]:checked').val());
            $('input.movil-final').val(getMovilFinal);
          }
        }
    });
    $('input[name="previous"]').click(function(){
      current_step = $(this).parents('fieldset');
      next_step = current_step.prev();
      next_step.show();
      current_step.hide();
      setProgressBar(--current);
    });
    setProgressBar(current);
    // Change progress bar action
    function setProgressBar(curStep){
      tabs.removeClass('active');
      switch (curStep) {
        case 1:
          $('.tab-configurador[step="1"]').addClass('active');
          break;
        case 2:
          $('.tab-configurador[step="2"]').addClass('active');
          break;
        case 3:
          $('.tab-configurador[step="3"]').addClass('active');
          break;
      }
    }
    function getMovilFinal() {
      var content = '', tarifa = 1;
      $('.smovil').each(function () {
        content += 'Tarifa '+tarifa+': '+$(this).find('.minutos h5').text()+' '+$(this).find('.datos p').text()+' | ';
        tarifa++;
      });
      return content;
    }
    function addRow(row, num) {
      row.clone(true, true).find('input').attr('id', 'toggle'+num ).parent().find('.button').attr('for', 'toggle'+num ).parent().appendTo(".grupomovil .selectmovil");
      var precio = row.find('.precio span').text();
      var id = 'toggle'+num;
      agregarMovil(precio, id);
    }
    function removeRow(rowdelete) {
      var id = rowdelete.attr('for');
      rowdelete.closest(".smovil").remove();
      content.find('.movil .'+id).remove();
    }
    $('.grupomovil').on('click', '.button', function (event) {
      var numlocal = $('.grupomovil .selectmovil > .smovil:last > input'),
	  smovil = $(".grupomovil .selectmovil .smovil"),
      num = parseInt( numlocal.prop("id").match(/\d+/g), 10 ) +1,
	  toggle = $(this).parent().find('input');
      if(smovil.length < 10 && toggle.is(':not(:checked)')) {
        addRow($(this).parent(), num);
      }else if (toggle.is(':checked') && smovil.length > 1){
        removeRow($(this));
        if(smovil.length === 10){
          $('.grupomovil .selectmovil > .smovil:last').find('input').prop("checked", false);
        }
      }else{
        const popperInstance2 = Popper.createPopper($(this)[0], tooltip2, {
          placement: 'right',
          modifiers: [
            {
              name: 'offset',
              options: {
                offset: [0, 8]
              }
            }
          ]
        });
        setTimeout(function() {
          show(event, tooltip2, popperInstance2);
        }, 100);
      }
      sumaTotal();
    });

    
  });
})(jQuery);






